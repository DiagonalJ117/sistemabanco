﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bootstrap_cols.aspx.cs" Inherits="VISTA.bootstrap_cols" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div class="container-fluid">
            <div class="row">
                <div class="col col-sm-3 col-md-2 col-lg-1">
                    <div class="card">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ornare in nunc ut faucibus. Duis tincidunt mattis rutrum. Suspendisse at tortor velit.
                    </div>
                </div>
                <div class="col col-sm-2 col-md-5 col-lg-1">
                    <div class="card">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ornare in nunc ut faucibus. Duis tincidunt mattis rutrum. Suspendisse at tortor velit.
                    </div>
                </div>
                <div class="col col-sm-1 col-md-4 col-lg-1">
                    <div class="card">
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Vivamus ornare in nunc ut faucibus. Duis tincidunt mattis rutrum. Suspendisse at tortor velit.
                    </div>
                </div>
            </div>
        </div>

                        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>

    </form>
</body>
</html>
