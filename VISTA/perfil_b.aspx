﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="perfil_b.aspx.cs" Inherits="VISTA.perfil_b" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>

        <!-- Bootstrap core CSS -->
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet"/>

    <!-- Custom fonts for this template -->
    <link href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Montserrat:400,700" rel="stylesheet" type="text/css"/>
    <link href="https://fonts.googleapis.com/css?family=Lato:400,700,400italic,700italic" rel="stylesheet" type="text/css"/>

    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css"/>
    <!-- Plugin CSS -->
    

    <!-- Custom styles for this template -->
    <link href="assets/perfil_b.css" rel="stylesheet" />

</head>
<body>
    <form id="form1" runat="server">
        <header class="bg-danger"><h1 class="text-center text-light">Mi Perfil</h1></header>
        <div class="container-fluid p-5" id="content">
            <div class="d-flex flex-row justify-content-between mb-5">
                <div class="card">
                    <div class="card-header">
                        <i class="fa fa-id-card"></i> Datos Generales
                    </div>
                    <img src="https://via.placeholder.com/150" class="card-img-top p-5 img-thumbnail" alt="...">
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Nombre: José Luis De La Torre Salas</li>
                            <li class="list-group-item">Edad: 24</li>
                            <li class="list-group-item">Ocupación: Desarrollador Web Independiente</li>
                        </ul>
                    </div>
                </div>
                 <div class="card">
                    <div class="card-header">
                        <i class="fa fa-id-card"></i> Desarrollo Web
                    </div>
                    
                    <div class="card-body">
                        <div class="row">
                        <div class="col">
                            <h3>Frameworks</h3>
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Django</li>
                            <li class="list-group-item">Laravel</li>
                            <li class="list-group-item">Ruby on Rails</li>
                            <li class="list-group-item">React</li>
                            <li class="list-group-item">Bootstrap</li>
                        </ul>
                        </div>
                         <div class="col">
                             <h3>Algunos Proyectos</h3>
                        <ul class="list-group list-group-flush">
                        <li class="list-group-item">Plataforma, Tienda en Línea para proyecto Milibromx</li>
                        <li class="list-group-item">Plataforma para negocio de reparación de Electronicos.</li>
                        <li class="list-group-item">Aplicación Web para administración de consultas y dietas de nutriologo. (Laravel)</li>
                        <li class="list-group-item">Plataforma para Egresados de Instituto Tecnológico de Hermosillo</li>
                        <li class="list-group-item">Portal para prestamo de proyectores en area de Administración de ITH.</li>
                        <li class="list-group-item">Portal para prestamo de Laboratorio de Computo de Edificio de Ciencias Económico-Administrativas en ITH.</li>
                        </ul>
                        </div>
                        </div>
                        <div class="d-flex flex-column">
                           <strong class="text-center">Repo en Gitlab: <a href="https://gitlab.com/FWC5757">https://gitlab.com/FWC5757</a> * </strong>
                            <p class="text-center">* = Algunos proyectos no se encuentran visibles debido a privacidad.</p>
                        </div>
                    </div>
                </div>
                

            </div>
                    <div class="card mb-5">
                    <div class="card-header">
                        <i class="fa fa-graduation-cap"></i> Escolaridad
                    </div>
                    <div class="card-body">
                        <ul class="list-group list-group-flush">
                            <li class="list-group-item">Primaria-Secundaria: Colegio Americano del Pacifico</li>
                            <li class="list-group-item">Preparatoria: Universidad TecMilenio Campus Hermosillo</li>
                            <li class="list-group-item">Universidad: 
                                <ul class="list-group">
                                    <li class="list-group-item">Universidad TecMilenio Campus Hermosillo: Lic. Diseño Gráfico y Animación <strong>2013 - 2015 (Hasta 5to semestre)</strong></li>
                                    <li class="list-group-item">Instituto Tecnológico de Hermosillo: Ing. Informática <strong>2016-presente</strong></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
            <div class="card mb-5">
                <div class="card-header">
                   <i class="fa fa-award"></i> Extras
                </div>
                <div class="card-body">
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">Diplomado en Creación y Desarrollo de Videojuegos en Unity Principante e Intermedio</li>
                        <li class="list-group-item">Diplomado en Animación 3D en Maya</li>
                        <li class="list-group-item">Certificación BULATS: Nivel C2</li>
                        <li class="list-group-item">Inglés 100%</li>
                        <li class="list-group-item">Conocimiento Intermedio de Raspberry Pi y Arduino</li>
                    </ul>
                </div>
            </div>
        </div>
        
    </form>
        <!-- Bootstrap core JavaScript -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
    
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>


    <!-- Plugin JavaScript -->


    <!-- Contact Form JavaScript -->

    <!-- Custom scripts for this template -->
    

</body>
</html>
