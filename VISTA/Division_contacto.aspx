﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Division_contacto.aspx.cs" Inherits="VISTA.Division_contacto" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link rel="stylesheet" href="/assets/styles_1.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="navbar">
            <ul>
              <li><a class="active" href="/Division.aspx">Inicio</a></li>
              <li><a href="#news">Actualizaciones</a></li>
              <li><a href="/Division_contacto.aspx">Contacto</a></li>
              <li><a href="#join">Unete a Nosotros</a></li>
            </ul>
        </div>
        <div id="shd_logo_container">
            <div class="flex-container">
            <img src="assets/the-division-shd-logo.jpg" alt="SHD" id="shd_logo"/>
             </div>
        </div>

        <div id="header">
            <h1>Strategic Homeland Division</h1>
        </div>
        <div id="content">
            <div class="desc_text" id="contacto">
                <h3>Contactanos</h3>
                <p>Envianos un mensaje y te apoyaremos en lo que podamos</p>
                <div class="flex-container contact-form-container">
                    
                    <form action="#">
                        <table id="contacto-form">
                        <tr>
                        <td><label for="email-input">Email</label></td>
                        <td><input type="email" id="email-input"/></td>
                        </tr>
                        <tr>

                        <td><label for="mensaje">Mensaje</label></td>
                        <td><textarea id="mensaje" rows="10"></textarea></td>
                        </tr>
                        <tr>
                        <td></td>
                        <td><input type="submit" value="Enviar" id="btn-enviar"/></td>
                        </tr>
                        </table>
                    </form>
                </div>
            </div>

        </div>
    </form>
</body>
</html>
