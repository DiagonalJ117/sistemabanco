﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Division.aspx.cs" Inherits="VISTA.Division" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <link rel="stylesheet" href="/assets/styles_1.css" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
        <div id="navbar">
            <ul>
              <li><a class="active" href="/Division.aspx">Inicio</a></li>
              <li><a href="#news">Actualizaciones</a></li>
              <li><a href="/Division_contacto.aspx">Contacto</a></li>
              <li><a href="#join">Unete a Nosotros</a></li>
            </ul>
        </div>
        <div id="shd_logo_container">
            <div class="flex-container">
            <img src="assets/the-division-shd-logo.jpg" alt="SHD" id="shd_logo"/>
             </div>
        </div>

        <div id="header">
            <h1>Strategic Homeland Division</h1>
        </div>
        <div id="content">
            <div class="desc_text" id="who_we_are">
                <h3>Quienes Somos?</h3>
                <p>Strategic Homeland Division, somos una unidad secreta del gobierno de E.E.U.U. altamente capacitada y entrenada para asegurar la continuidad del gobierno en casos de <strong>emergencia catastrofica</strong></p>
            </div>
            <div class="desc_text" id="localizaciones">
                <h3>Donde nos encontramos?</h3>
                <p>Cubrimos todo territorio estadounidense. Sin embargo, nos encontramos en territorio internacional</p>
                <h3>Nuestras Sedes</h3>
                <div class="flex-container location-container">
                    
                    <div class="location">
                        <h4>Washington</h4>
                        <img src="/assets/washington.jpg" alt="Washington"/>
                    </div>
                   <div class="location">
                        <h4>Manhattan</h4>
                        <img src="/assets/manhattan.jpg" alt="Manhattan"/>
                    </div>
                    <div class="location">
                        <h4>Seattle</h4>
                        <img src="/assets/seattle.jpg" alt="Seattle"/>
                    </div>
                    <div class="location">
                        <h4>Los Angeles</h4>
                        <img src="/assets/losangeles.jpg" alt="Los Angeles"/>
                    </div>
                </div>
            </div>
            <div class="desc_text" id="what_we_do">
                 <h3>Qué hacemos?</h3>
                 <ul>
                     <li>Rescate de Personas</li>
                     <li>Apoyo a comunidades</li>
                     <li>Primeros Auxilios</li>
                     <li>Brigadas de Defensa</li>
                     <li>Seguridad de VIPs</li>
                     
                 </ul>
            </div>

        </div>
    </form>
</body>
</html>
