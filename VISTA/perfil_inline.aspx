﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="perfil_inline.aspx.cs" Inherits="VISTA.perfil_inline" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
</head>
<body style="font-family:Arial; background-color:#aef0f7;">
    <form id="form1" runat="server">
        <div>
            <div id="header" style="text-align:center;">
                <h1>José Luis De La Torre Salas</h1>
                <strong>Repo en Gitlab: <a href="https://gitlab.com/FWC5757">https://gitlab.com/FWC5757</a> * </strong>
                <p>* = Algunos proyectos no se encuentran visibles debido a privacidad.</p>

            </div>
            <div id="content">
                <h3 style="padding: 10px; background-color:#bef2f7; border-bottom: 5px solid black;"><img src="/assets/form.png" height="50px"/> Datos Generales</h3>

                <ul id="lista-generales" style="background-color:#aef0f7; padding:20px; list-style-type: none;">
                    <li>Edad: 24</li>
                    <li>Ocupación: Desarrollador Web Independiente</li>
                </ul>


                <h3 style="padding: 10px; background-color:#bef2f7; border-bottom: 5px solid black;"><img src="/assets/college-graduation.png" height="50px" /> Escolaridad</h3>
                <ul id="lista-escolaridad" style="list-style-type: none; background-color: #aef0f7; padding: 20px;">

                    <li>Primaria-Secundaria: Colegio Americano del Pacifico</li>
                    <li>Preparatoria: Universidad TecMilenio Campus Hermosillo</li>
                    <li>Universidad: 
                                <ul>
                                    <li>Universidad TecMilenio Campus Hermosillo: Lic. Diseño Gráfico y Animación <strong>2013 - 2015 (Hasta 5to semestre)</strong></li>
                                    <li>Instituto Tecnológico de Hermosillo: Ing. Informática <strong>2016-presente</strong></li>
                                </ul>
                    </li>
                </ul>
                <h3 style ="padding: 10px; background-color:#bef2f7; border-bottom: 5px solid black;"><img src="/assets/badge.png" height="50px"/> Extras</h3>

                <ul id="lista-extras" style="background-color: #aef0f7; padding: 20px; list-style-type: none;">
                    <li>Diplomado en Creación y Desarrollo de Videojuegos en Unity Principante e Intermedio</li>
                    <li>Diplomado en Animación 3D en Maya</li>
                    <li>Certificación BULATS: Nivel C2</li>
                    <li>Inglés 100%</li>
                    <li>Conocimiento Intermedio de Raspberry Pi y Arduino</li>
                </ul>
            </div>
        </div>
         <div style="display:flex; flex-direction: row; justify-content:center;">
            <div id="lista-frameworks">
                <h3 class="lista-titulo" style="text-align:center;padding: 10px; background-color:#bef2f7; border-bottom: 5px solid black;">Frameworks utilizadas</h3>
                    <ul style="list-style-type: circle;">
                        <li>Django</li>
                        <li>Laravel</li>
                        <li>Ruby on Rails</li>
                        <li>React</li>
                        <li>Bootstrap</li>
                    </ul>
                
            </div>
            <div id="lista-proyectos">
                <h3 class="lista-titulo" style="text-align:center;padding: 10px; background-color:#bef2f7; border-bottom: 5px solid black;">Algunos Proyectos</h3>
                    <ul style="list-style-type: circle;">
                        <li>Plataforma, Tienda en Línea para proyecto Milibromx</li>
                        <li>Plataforma para negocio de reparación de Electronicos.</li>
                        <li>Aplicación Web para administración de consultas y dietas de nutriologo. (Laravel)</li>
                        <li>Plataforma para Egresados de Instituto Tecnológico de Hermosillo</li>
                        <li>Portal para prestamo de proyectores en area de Administración de ITH.</li>
                        <li>Portal para prestamo de Laboratorio de Computo de Edificio de Ciencias Económico-Administrativas en ITH.</li>
                    </ul>
               </div>
            </div>

        <h3 style="padding: 10px; background-color:#bef2f7; border-bottom: 5px solid black;"><img src="/assets/badge.png" height="50px"/> Contacto</h3>
        <form action="#">
            <table id="contacto-form" style="background-color: #aef0f7; padding: 20px;">
            <tr>
            <td><label for="email-input">Email</label></td>
            <td><input type="email" id="email-input"/></td>
            </tr>
            <tr>

            <td><label for="mensaje">Mensaje</label></td>
            <td><textarea id="mensaje" rows="10"></textarea></td>
            </tr>
            <tr>
            <td></td>
            <td><input type="submit" value="Enviar"/></td>
            </tr>
            </table>
        </form>
    </form>
</body>
</html>
