﻿

var ctx_d1 = document.getElementById('cht_diario_1');
var ctx_d2 = document.getElementById('cht_diario_2');
var ctx_d3 = document.getElementById('cht_diario_3');
var ctx_m1 = document.getElementById('cht_mensual_1');
var ctx_m2 = document.getElementById('cht_mensual_2');
var ctx_m3 = document.getElementById('cht_mensual_3');
var ctx_a1 = document.getElementById('cht_anual_1');
var ctx_a2 = document.getElementById('cht_anual_2');
var ctx_a3 = document.getElementById('cht_anual_3');

function random_rgba() {
    var o = Math.round, r = Math.random, s = 255;
    return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(1) + ')';
}

Array.from({ length: 40 }, () => random_rgba());

var chart1 = new Chart(ctx_d1, {
    type: 'bar',
    data: {
        labels: ['Semana 1', 'Semana 2', 'Semana 3', 'Semana 4', 'Semana 5', 'Semana 6'],
        datasets: [{
            label: 'Nuevos Clientes',
            data: Array.from({ length: 6 }, () => Math.floor(Math.random() * 500)),
            backgroundColor: Array.from({ length: 6 }, () => random_rgba()),
            borderColor: Array.from({ length: 6 }, () => random_rgba()),
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
var chart2 = new Chart(ctx_d2, {
    type: 'pie',
    data: {
        labels: ['Regulares', 'VIP', 'Oro', 'Platino', 'Diamante', 'Otros'],
        datasets: [{
            label: 'Nuevos Clientes',
            data: Array.from({ length: 6 }, () => Math.floor(Math.random() * 500)),
            backgroundColor: Array.from({ length: 6 }, () => random_rgba()),
            borderColor: Array.from({ length: 6 }, () => random_rgba()),
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
var chart3 = new Chart(ctx_d3, {
    type: 'line',
    data: {
        labels: ['Lunes', 'Martes', 'Miercoles', 'Jueves', 'Viernes', 'Sabado'],
        datasets: [{
            label: 'Clientes en la semana',
            data: Array.from({ length: 6 }, () => Math.floor(Math.random() * 500)),
            backgroundColor: Array.from({ length: 6 }, () => random_rgba()),
            borderColor: Array.from({ length: 6 }, () => random_rgba()),
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var chart4 = new Chart(ctx_m1, {
    type: 'bar',
    data: {
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        datasets: [{
            label: 'Clientes por Mes',
            data: Array.from({ length: 12 }, () => Math.floor(Math.random() * 500)),
            backgroundColor: Array.from({ length: 12 }, () => random_rgba()),
            borderColor: Array.from({ length: 12 }, () => random_rgba()),
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
var chart5 = new Chart(ctx_m2, {
    type: 'doughnut',
    data: {
        labels: ['Regulares', 'VIP', 'Oro', 'Platino', 'Diamante', 'Otros'],
        datasets: [{
            label: 'Nuevos Clientes por Tipo',
            data: Array.from({ length: 6 }, () => Math.floor(Math.random() * 500)),
            backgroundColor: Array.from({ length: 6 }, () => random_rgba()),
            borderColor: Array.from({ length: 6 }, () => random_rgba()),
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
var chart6 = new Chart(ctx_m3, {
    type: 'radar',
    data: {
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        datasets: [{
            label: 'Clientes por Mes',
            data: Array.from({ length: 12 }, () => Math.floor(Math.random() * 500)),
            backgroundColor: Array.from({ length: 12 }, () => random_rgba()),
            borderColor: Array.from({ length: 12 }, () => random_rgba()),
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var chart7 = new Chart(ctx_a1, {
    type: 'bar',
    data: {
        labels: ['2014', '2015', '2016', '2017', '2018', '2019'],
        datasets: [{
            label: 'Clientes en los ultimos 6 Años',
            data: Array.from({ length: 6 }, () => Math.floor(Math.random() * 3000)),
            backgroundColor: Array.from({ length: 6 }, () => random_rgba()),
            borderColor: Array.from({ length: 6 }, () => random_rgba()),
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

var chart8 = new Chart(ctx_a2, {
    type: 'doughnut',
    data: {
        labels: ['Regulares', 'VIP', 'Oro', 'Platino', 'Diamante', 'Otros'],
        datasets: [{
            label: 'Tipo de Cliente en el Año',
            data: Array.from({ length: 6 }, () => Math.floor(Math.random() * 3000)),
            backgroundColor: Array.from({ length: 6 }, () => random_rgba()),
            borderColor: Array.from({ length: 6 }, () => random_rgba()),
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});
var chart9= new Chart(ctx_a3, {
    type: 'polarArea',
    data: {
        labels: ['Enero', 'Febrero', 'Marzo', 'Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'],
        datasets: [{
            label: 'Clientes por Mes',
            data: Array.from({ length: 12 }, () => Math.floor(Math.random() * 500)),
            backgroundColor: Array.from({ length: 12 }, () => random_rgba()),
            borderColor: Array.from({ length: 12 }, () => random_rgba()),
            borderWidth: 1
        }]
    },
    options: {
        scales: {
            yAxes: [{
                ticks: {
                    beginAtZero: true
                }
            }]
        }
    }
});

