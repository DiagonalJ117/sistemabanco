﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="perfil_interno.aspx.cs" Inherits="VISTA.perfil_interno" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <style type="text/css">
        body {
            font-family:Arial;
            background-color:#aef0f7;
        }



        #header{
            text-align:center;
        }


        ul {
            list-style-type: none;
        }



        #lista-extras, #lista-frameworks, #lista-generales, #lista-proyectos {
            background-color: #aef0f7;
            padding: 20px;
        }


        #lista-frameworks > td > ul {
            list-style-type: circle;
        }


        #lista-proyectos > td > ul {
            list-style-type: circle;
        }

        #contacto-form {
            background-color: #aef0f7;
            padding: 20px;
        }

        h3{
            padding: 10px;
            background-color:#bef2f7;
            border-bottom: 5px solid black;
        }

        .flex-container{
            display:flex;
            flex-direction: row;
            justify-content:center;

        }

        .lista-titulo{
            text-align:center;
        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <div id="header">
                <h1>José Luis De La Torre Salas</h1>
                <strong>Repo en Gitlab: <a href="https://gitlab.com/FWC5757">https://gitlab.com/FWC5757</a> * </strong>
                <p>* = Algunos proyectos no se encuentran visibles debido a privacidad.</p>

            </div>
            <div id="content">
                <h3><img src="/assets/form.png" height="50px"/> Datos Generales</h3>

                <ul id="lista-generales">
                    <li>Edad: 24</li>
                    <li>Ocupación: Desarrollador Web Independiente</li>
                </ul>


                <h3><img src="/assets/college-graduation.png" height="50px" /> Escolaridad</h3>
                <ul id="lista-escolaridad">

                    <li>Primaria-Secundaria: Colegio Americano del Pacifico</li>
                    <li>Preparatoria: Universidad TecMilenio Campus Hermosillo</li>
                    <li>Universidad: 
                                <ul>
                                    <li>Universidad TecMilenio Campus Hermosillo: Lic. Diseño Gráfico y Animación <strong>2013 - 2015 (Hasta 5to semestre)</strong></li>
                                    <li>Instituto Tecnológico de Hermosillo: Ing. Informática <strong>2016-presente</strong></li>
                                </ul>
                    </li>
                </ul>
                <h3><img src="/assets/badge.png" height="50px"/> Extras</h3>

                <ul id="lista-extras">
                    <li>Diplomado en Creación y Desarrollo de Videojuegos en Unity Principante e Intermedio</li>
                    <li>Diplomado en Animación 3D en Maya</li>
                    <li>Certificación BULATS: Nivel C2</li>
                    <li>Inglés 100%</li>
                    <li>Conocimiento Intermedio de Raspberry Pi y Arduino</li>
                </ul>
            </div>
        </div>
         <div class="flex-container">
            <div id="lista-frameworks">
                <h3 class="lista-titulo">Frameworks utilizadas</h3>
                    <ul>
                        <li>Django</li>
                        <li>Laravel</li>
                        <li>Ruby on Rails</li>
                        <li>React</li>
                        <li>Bootstrap</li>
                    </ul>
                
            </div>
            <div id="lista-proyectos">
                <h3 class="lista-titulo">Algunos Proyectos</h3>
                    <ul>
                        <li>Plataforma, Tienda en Línea para proyecto Milibromx</li>
                        <li>Plataforma para negocio de reparación de Electronicos.</li>
                        <li>Aplicación Web para administración de consultas y dietas de nutriologo. (Laravel)</li>
                        <li>Plataforma para Egresados de Instituto Tecnológico de Hermosillo</li>
                        <li>Portal para prestamo de proyectores en area de Administración de ITH.</li>
                        <li>Portal para prestamo de Laboratorio de Computo de Edificio de Ciencias Económico-Administrativas en ITH.</li>
                    </ul>
               </div>
            </div>

        <h3><img src="/assets/badge.png" height="50px"/> Contacto</h3>
        <form action="#">
            <table id="contacto-form">
            <tr>
            <td><label for="email-input">Email</label></td>
            <td><input type="email" id="email-input"/></td>
            </tr>
            <tr>

            <td><label for="mensaje">Mensaje</label></td>
            <td><textarea id="mensaje" rows="10"></textarea></td>
            </tr>
            <tr>
            <td></td>
            <td><input type="submit" value="Enviar"/></td>
            </tr>
            </table>
        </form>
    </form>
</body>
</html>
