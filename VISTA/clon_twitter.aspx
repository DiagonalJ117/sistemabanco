﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="clon_twitter.aspx.cs" Inherits="VISTA.clon_twitter" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title></title>
    <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" />
    <link href="assets/clon_twitter.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.2.0/css/all.css" />
</head>

<body>
    <form id="form1" runat="server">
<nav class="navbar navbar-expand-lg fixed-top">
  <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="container collapse navbar-collapse">
    <!-- Navbar navigation links -->
    <ul class="navbar-nav mr-auto">
      <li class="nav-item active">
        <a class="nav-link" href="#"><i class="fas fa-home" aria-hidden="true"></i> Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-bolt"></i> Moments</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-bell"></i> Notifications</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"><i class="fas fa-inbox"></i> Messages</a>
      </li>
    </ul>
    <!-- END: Navbar navigation links -->
    <!-- Navbar Search form -->
    <form class="navbar-form" role="search">
      <div class="input-group">
        <input type="text" class="form-control input-search" placeholder="Search Twitter" name="srch-term" id="srch-term">
        <div class="input-group-btn">
          <button class="btn btn-default btn-search" type="submit"><i class="fas fa-search navbar-search-icon"></i></button>
        </div>
      </div>
    </form>
    <!-- END: Navbar Search form -->
    <!-- Navbar User menu -->
    <div class="dropdown navbar-user-dropdown">
      <button class="btn btn-secondary dropdown-toggle btn-circle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"></button>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="#">Action</a>
        <a class="dropdown-item" href="#">Another action</a>
        <a class="dropdown-item" href="#">Something else here</a>
      </div>
    </div>
    <!-- END: Navbar User menu -->
    <!-- Navbar Tweet button -->
    <button class="btn btn-search-bar">Tweet</button>
  </div>
</nav>
<!-- END: Fixed top navbar -->
<div class="main-container">
  <!-- Profile background large image -->
  <div class="row profile-background">
    <div class="container">
	  <!-- User main avatar -->
      <div class="avatar-container">
        <div class="avatar">

        </div>
      </div>
    </div>
  </div>

  <nav class="navbar navbar-expand profile-stats">
    <div class="container">
      <div class="row">

        <div class="col">
          <ul>
            <li class="profile-stats-item-active">
              <a>
                <span class="profile-stats-item profile-stats-item-label">Tweets</span>
                <span class="profile-stats-item profile-stats-item-number">51</span>
              </a>
            </li>
            <li>
              <a>
                <span class="profile-stats-item profile-stats-item-label">Following</span>
                <span class="profile-stats-item profile-stats-item-number">420</span>
              </a>
            </li>
            <li>
              <a>
                <span class="profile-stats-item profile-stats-item-label">Followers</span>
                <span class="profile-stats-item profile-stats-item-number">583</span>
              </a>
            </li>
            <li>
              <a>
                <span class="profile-stats-item profile-stats-item-label">Likes</span>
                <span class="profile-stats-item profile-stats-item-number">241</span>
              </a>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </nav>
  <div class="container main-content">
    <div class="row">
      <div class="col profile-col">
        <!-- Left column -->
        <div class="profile-header">
          <!-- Header information -->
          <h3 class="profile-fullname"><a>JS<a></h3>
          <h2 class="profile-element"><a>@J117</a></h2>
          <a class="profile-element profile-website" href="#"><i class="fas fa-link"></i>&nbsp;https://gitlab.com/FWC5757</a>
          <a class="profile-element profile-website" href="#"><i class="fas fa-location"></i>&nbsp;Por Ahi</a>
          <h2 class="profile-element"><i class="fas fa-calendar"></i>Joined November 2012</h2>
          <button class="btn btn-search-bar tweet-to-btn">Tweet to JS</button>
          <a class="profile-element profile-website" href="#"><i class="fas fa-file-media"></i>1,142 Photos and videos</a>
          <div class="pic-grid">
            <!-- Image grid -->
            <div class="row">
              <div class="col pic-col"><img src="assets/R6S-Mozzie.png" height="73px" class=""></div>
              <div class="col pic-col"><img src="https://www.placehold.it/50" height="73px" class=""></div>
              <div class="col pic-col"><img src="https://www.placehold.it/50" height="73px" class=""></div>
            </div>
            <!-- End: row -->
            <div class="row">
              <div class="col pic-col"><img src="https://www.placehold.it/50" height="73px" class=""></div>
              <div class="col pic-col"><img src="https://www.placehold.it/50" height="73px" class=""></div>
              <div class="col pic-col"><img src="https://www.placehold.it/50" height="73px" class=""></div>
            </div>
            <!-- End: row -->
          </div>
          <!-- End: image grid -->
        </div>
      </div>
      <!-- End; Left column -->
      <!-- Center content column -->
      <div class="col-6">
        <ol class="tweet-list listatweets">
          <li class="tweet-card untweet">
            <div class="tweet-content">
              <div class="tweet-header">
                <span class="fullname">
                  <strong>José Luis De La Torre</strong>
                </span>
                <span class="username">@J117</span>
                <span class="tweet-time">- Jul 18</span>
              </div>
              <a>
                <img class="tweet-card-avatar" src="https://www.placehold.it/50" alt="">
              </a>
              <div class="tweet-text">
                <p class="" lang="es" data-aria-label-part="0">¡Nuevo artículo en Mozilla!<br>Resuelto: Corregido – Una breve historia sobre un error reportado por la comunidad <a href="https://t.co/dqg5hVQXA0" class="twitter-timeline-link" target="_blank"><span class="">https://www.mozilla-hispano.org/</span></a>                  <a href="" class="twitter-hashtag"><s>#</s><b>firefox</b></a> <a href="" class="twitter-hashtag"><s>#</s><b>comunidad</b></a>
                  <a href="" class="twitter-hashtag" dir="ltr"></a>
                </p>
              </div>
              <div class="tweet-footer">
                <a class="tweet-footer-btn">
                  <i class="fas fa-comment" aria-hidden="true"></i><span> 18</span>
                </a>
                <a class="tweet-footer-btn">
                  <i class="fas fa-sync" aria-hidden="true"></i><span> 64</span>
                </a>
                <a class="tweet-footer-btn">
                  <i class="fas fa-heart" aria-hidden="true"></i><span> 202</span>
                </a>
                <a class="tweet-footer-btn">
                  <i class="fas fa-mail" aria-hidden="true"></i><span> 155</span>
                </a>
              </div>
            </div>
          </li>

        </ol>
        <!-- End: tweet list -->
      </div>
      <!-- End: Center content column -->
      <div class="col right-col">
        <div class="content-panel">
          <div class="panel-header">
            <h4>Who to follow</h4><small><a href="#">Refresh</a></small><small><a href="#">View all</a></small>
          </div>
          <!-- Who to Follow panel -->
          <div class="panel-content">
            <!--Follow list -->
            <ol class="tweet-list">
              <li class="tweet-card">
                <div class="tweet-content">
                  <img class="tweet-card-avatar" src="/assets/R6S-Mozzie.png" alt="">
                  <div class="tweet-header">
                    <span class="fullname">
                  <strong>JS</strong>
                </span>
                    <span class="username">@J117</span>
                  </div>

                  <button class="btn btn-follow">Follow</button>
                </div>
              </li>
              <li class="tweet-card">
                <div class="tweet-content">
                  <img class="tweet-card-avatar" src="/assets/R6S-Mozzie.png" alt="">
                  <div class="tweet-header">
                    <span class="fullname">
                  <strong>JS</strong>
                </span>
                    <span class="username">@J117</span>
                  </div>

                  <button class="btn btn-follow">Follow</button>
                </div>
              </li>
            </ol>
            <!--END: Follow list -->
          </div>
        </div>
      </div>
    </div>
  </div>

    
                <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src='https://npmcdn.com/tether@1.2.4/dist/js/tether.min.js'></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js" integrity="sha384-smHYKdLADwkXOn1EmN1qk/HfnUcbVRZyYmZ4qpPea6sjB/pTJ0euyQp0Mk8ck+5T" crossorigin="anonymous"></script>
    <script src="assets/clon_twitter.js"></script>
    </form>
</body>
</html>
